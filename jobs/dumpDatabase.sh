#!/bin/sh

DUMP_FILE_NAME="backupOn`date +%Y-%m-%d-%H-%M`.dump"
echo "Creating dump: $DUMP_FILE_NAME"

MYSQL_HOST='mariadb'
MYSQL_DATABASE='snipeit'
MYSQL_USER='snipeit'
MYSQL_PASSWORD='changeme1234'
BACKUPDIR=/var/backup/mysql

mkdir $BACKUPDIR
cd $BACKUPDIR

mysqldump -h $MYSQL_HOST -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE > $DUMP_FILE_NAME

if [ $? -ne 0 ]; then
  rm $DUMP_FILE_NAME
  echo "Back up not created, check db connection settings"
  exit 1
fi

echo 'Successfully Backed Up'
exit 0