### STEP 1 
#### Application selection:
https://github.com/snipe/snipe-it


### STEP 2 
#### Integration with git:
https://docs.gitlab.com

-----
### STEP 3 
#### Started with Google Cloud
https://cloud.google.com/docs
#### Installing Google Cloud SDK
https://cloud.google.com/sdk/docs/install#linux
#### Installing kubectl
```bash
sudo apt install kubectl
```
-----
### STEP 4 
#### Create GKE clusters
https://docs.gitlab.com/ee/user/project/clusters/add_gke_clusters.html#gke-requirements

------
### STEP 5 
#### Connect GKE clusters
```bash
gcloud init
gcloud container clusters get-credentials <your_cluster>
```
------
### STEP 6 
#### Create builder
##### Create VM

https://cloud.google.com/compute/docs/instances/create-start-instance

##### Install docker, kubectl

https://docs.docker.com/engine/install/ubuntu/

##### Install gitlab-runner
```bash
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
export GITLAB_RUNNER_DISABLE_SKEL=true; yes | sudo apt-get install gitlab-runner

sudo gitlab-runner register  --non-interactive   --url "https://gitlab.com/"   --registration-token “XAE3A6qRLzbzhWsTryVa”  --executor "shell"  --name "gcp-2021" --tag-list "docker,builder”  --run-untagged="true" --locked="false"

gitlab-runner register --non-interactive --executor shell --url 'https://gitlab.com/' --registration-token 'YOUR_KEY_GITLAB'
```
##### Create builder
```bash
sudo su 
su gitlab-runner
gcloud init
gcloud container clusters get-credentials <your_cluster>
```
------
## Kubernetes
### STEP 7
##### Create persistent volumes
claim
```yaml
# pvc-demo.yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-database
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
```
database

kubectl apply -f database.yaml

------
### STEP 8
##### Create Deployments

kubectl apply -f application.yaml

------
### STEP 9
##### Create Service

kubectl apply -f service.yaml

------
### STEP 10 
#### Create CI\CD

kubectl apply -f .gitlab-ci.yml

------
### STEP 11
##### Create BackUp

 kubectl apply -f cronjob.yaml 

------
### STEP 12
##### Create Variables
https://docs.gitlab.com/ee/ci/variables/

------
### STEP 13
##### Настройка логирования и мониторинга для указанных сервисов
